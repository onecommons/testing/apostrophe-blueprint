# README

This Unfurl project provides a template for instantiating a [MongoDBWebApp](https://gitlab.com/onecommons/unfurl-types/-/blob/main/service-template.yaml#L88) -- see `ensemble-template.yaml`. You can override any of its resource template settings in the `ensemble.yaml` that is generated when you clone this project.

## Installation

1. Make sure you have the very latest Unfurl: `pip3 install -U -e "git+https://github.com/onecommons/unfurl.git#egg=unfurl"`

2. Clone this project: "unfurl clone https://gitlab.com/onecommons/testing/webapp-example.git"

3. The template creates a compute instance on Google Cloud so you need to have a valid connection to a Google Cloud Project (e.g.
by setting the `GOOGLE_APPLICATION_CREDENTIALS` environment variable).

4. This template create a subdomain on the "untrusted.me" domain, which is managed by our Digital Ocean account. This requires you to set a DIGITALOCEAN_TOKEN environment variable for that account.

## Deploying

5. Run `unfurl deploy` to deploy.

This will create a GCP compute instance using an nginx container image.

* Expose the container's port over https (using Traefik).
* Provision a https certificate using Let's Encrypt.
* Register the deployed server on untrusted.me -- the URL will outputed when deploy completes.

After the deploy job completes it usually takes a couple of minutes before the nginx server is accessible in your web browser (it needs the Let's Encrypt certificate provisioning to finish).
